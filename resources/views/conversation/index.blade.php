@extends('layouts.app')


@section('content')

  <div class="container">
    <h1>Message chat area</h1>
    <chat-component user_from="{{Auth::id()}}" conversation_id="{{$id}}"></chat-component>
  </div>

@stop
