<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationUser extends Model
{
    public function users(){
      return $this->belongsTo('App\User','user_1');
    }
    public function users2(){
      return $this->belongsTo('App\User','user_2');
    }
}
