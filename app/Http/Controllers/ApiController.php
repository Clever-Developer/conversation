<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MessageUser;
use App\ConversationUser;

class ApiController extends Controller
{
    public function send(Request $request){
      $conversation = ConversationUser::find($request->conversation_id);
      $to_id = $conversation->user_1 == $request->user_from ? $conversation->user_2 : $conversation->user_1;
      $message = new MessageUser();
      $message->sender_id = $request->user_from;
      $message->to_id = $to_id;
      $message->message = $request->message;
      $message->conversation_id = $request->conversation_id;
      $message->save();

      $chat = MessageUser::where('id', $message->id)->with('users')->first();
      return response()->json($chat);
    }
    public function getMessages(Request $request){
      $messages = MessageUser::where('conversation_id', $request->conversation_id)->with('users')->get();
      return response()->json($messages);
    }

    public function conversations(Request $request){
      $conversations = ConversationUser::where('user_1', $request->user_from)->orWhere('user_2', $request->user_from)->get();
      $array = [];
      foreach($conversations as $key => $chat){
        $to_id = $chat->user_1 == $request->user_from ? $chat->user_2 : $chat->user_1;
        $array[] = ['conversation_id' => $chat->id, 'user_name'=> $this->userName($to_id)];
      }

      return response()->json($array);
    }

    protected function userName($id){
      return \App\User::find($id)->name;
    }
}
