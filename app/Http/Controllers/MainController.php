<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(){
      return view('messages.index');
    }

    public function conversation($id){
      return view('conversation.index', compact('id'));
    }
}
